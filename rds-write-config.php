<!DOCTYPE html>
<html>
<head>
  <title>Infrastructure 404AZ Website</title>
  <meta http-equiv="refresh" content="10,URL=/rds.php" />
  <link href="css/bootstrap.min.css" rel="stylesheet">
  <link href="css/style.css" rel="stylesheet">
</head>

  <body>
    <div class="container">

      <div class="row">
      <div class="col-md-12">
      <?php include('menu.php'); ?>

      <div class="jumbotron">

      <?php
	  ini_set('display_errors', 1);
      ini_set('display_startup_errors', 1);
      error_reporting(E_ALL);
        $ep = $_POST['endpoint'];
      	$db = $_POST['database'];
        $un = $_POST['username'];
        $pw = $_POST['password'];
		$port = "3306";

        $con = mysqli_connect($ep, $un, $pw, $db, $port);
        if(mysqli_connect_errno()) {
			echo "Failed to connect to MySQL: " . mysqli_connect_error();
			exit;
        }
		
	    echo "<br /><p>Writing the database</p>";
		$sql_drop = "DROP TABLE IF EXISTS address";
		$sql_table = "CREATE TABLE address(id INT(4) NOT NULL AUTO_INCREMENT PRIMARY KEY,
		lastname VARCHAR(30), 
		firstname VARCHAR(30), 
		phone VARCHAR(30), 
		email VARCHAR(30));";
			
		$sql_data = "INSERT INTO address (lastname, firstname, phone, email) VALUES 
		( 'Johnson', 'Roberto', '123-456-7890', 'robertoj@someaddress.com'), 
		( 'Doe', 'Jane', '010-110-1101', 'janed@someotheraddress.org');";
		
		mysqli_query($con, $sql_drop) or die(mysqli_error($con));
		mysqli_query($con, $sql_table) or die(mysqli_error($con));
		mysqli_query($con, $sql_data) or die(mysqli_error($con));
		echo "<br /><p>Writing config out to rds.conf.php </p>";

		$rds_conf_file = 'rds.conf.php';
		$handle = fopen($rds_conf_file, 'w') or die('Cannot open file:  '.$rds_conf_file);
		$data = "<?php \$RDS_URL='" . $ep . "'; \$RDS_DB='" . $db . "'; \$RDS_user='" . $un . "'; \$RDS_pwd='" . $pw . "'; \$PORT='" . $port . "';?>";
		fwrite($handle, $data);
        fclose($handle);

        mysqli_close($con);

        echo "<br /><br /><p><i>Redirecting to rds.php in 10 seconds (or click <a href=rds.php>here</a>)</i></p>";


      ?>

    </div>
    </div>
  </div>
  </div>

    <script src="js/jquery.min.js"></script>
    <script src="js/bootstrap.min.js"></script>
    <script src="js/scripts.js"></script>
  </body>
</html>
